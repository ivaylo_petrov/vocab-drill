package com.ekezet.vocabdrill.widgets;

import java.text.ParseException;
import java.util.Calendar;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.kvtml.interval.DateItem;
import com.ekezet.vocabdrill.kvtml.interval.ParsedInterval;

public class IntervalPreference extends DialogPreference
{
	private static final String ANDROID_NS = "http://schemas.android.com/apk/res/android";

	private ParsedInterval mInterval;

	private EditText mMonthsEdit;
	private EditText mDaysEdit;
	private EditText mHoursEdit;

	private Calendar mCalendar;
	private String mLastInterval = "";

	protected class IncrementClickListener implements OnClickListener
	{
		protected DateItem mPart;
		protected int mMax;

		public IncrementClickListener(final DateItem part, final int max)
		{
			mPart = part;
			mMax = max;
		}

		@Override
		public void onClick(View v)
		{
			int x = mInterval.getPart(mPart);
			if (x < mMax)
				x = x + 1;
			else return;
			mInterval.set(mPart, x);
			updateTexts();
		}
	}

	protected class DecrementClickListener implements OnClickListener
	{
		protected DateItem mPart;
		protected int mMin;

		public DecrementClickListener(final DateItem part, final int min)
		{
			mPart = part;
			mMin = min;
		}

		public DecrementClickListener(final DateItem part)
		{
			mPart = part;
			mMin = 0;
		}

		@Override
		public void onClick(View v)
		{
			int x = mInterval.getPart(mPart);
			if (mMin < x)
				x = x - 1;
			else return;
			mInterval.set(mPart, x);
			updateTexts();
		}
	}

	public IntervalPreference(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		final String dfi = attrs.getAttributeValue(ANDROID_NS, "defaultValue");
		if (shouldPersist())
			mInterval = new ParsedInterval(getPersistedString(dfi));
		else mInterval = new ParsedInterval(dfi);
	}

	@Override
	protected View onCreateDialogView()
	{
		super.onCreateDialogView();
		final LayoutInflater inflater =
			(LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View dlg = inflater.inflate(R.layout.dialog_interval_select, null);

		// edit text boxes
		mMonthsEdit = (EditText) dlg.findViewById(R.id.interval_months_text);
		mDaysEdit = (EditText) dlg.findViewById(R.id.interval_days_text);
		mHoursEdit = (EditText) dlg.findViewById(R.id.interval_hours_text);

		Button btn = null;
		// months
		btn = (Button) dlg.findViewById(R.id.interval_months_up);
		btn.setOnClickListener(new IncrementClickListener(DateItem.MONTHS, 12));
		btn = (Button) dlg.findViewById(R.id.interval_months_down);
		btn.setOnClickListener(new DecrementClickListener(DateItem.MONTHS));
		// days
		btn = (Button) dlg.findViewById(R.id.interval_days_up);
		btn.setOnClickListener(new IncrementClickListener(DateItem.DAYS, 31));
		btn = (Button) dlg.findViewById(R.id.interval_days_down);
		btn.setOnClickListener(new DecrementClickListener(DateItem.DAYS));
		// hours
		btn = (Button) dlg.findViewById(R.id.interval_hours_up);
		btn.setOnClickListener(new IncrementClickListener(DateItem.HOURS, 24));
		btn = (Button) dlg.findViewById(R.id.interval_hours_down);
		btn.setOnClickListener(new DecrementClickListener(DateItem.HOURS));
		updateTexts();

		return dlg;
	}

	@Override
	public CharSequence getSummary()
	{
		if (!updateCalendar())
			return "...";
		// get calendar with interval
		String summary = "";
		if (mCalendar.get(Calendar.MONTH) != 0)
		{
			if (mCalendar.get(Calendar.MONTH) == 1)
				summary += mCalendar.get(Calendar.MONTH) + " month";
			else summary += mCalendar.get(Calendar.MONTH) + " months";
			if (mCalendar.get(Calendar.DAY_OF_MONTH) - 1 != 0 || mCalendar.get(Calendar.HOUR_OF_DAY) != 0)
				summary += ", ";
		}
		if (mCalendar.get(Calendar.DAY_OF_MONTH) - 1 != 0)
		{
			if (mCalendar.get(Calendar.DAY_OF_MONTH) - 1 == 1)
				summary += mCalendar.get(Calendar.DAY_OF_MONTH) - 1 + " day";
			else summary += mCalendar.get(Calendar.DAY_OF_MONTH) - 1 + " days";
			if (mCalendar.get(Calendar.HOUR_OF_DAY) != 0)
				summary += ", ";
		}
		if (mCalendar.get(Calendar.HOUR_OF_DAY) != 0)
		{
			if (mCalendar.get(Calendar.HOUR_OF_DAY) == 1)
				summary += mCalendar.get(Calendar.HOUR_OF_DAY) + " hour";
			else summary += mCalendar.get(Calendar.HOUR_OF_DAY) + " hours";
		}
		return summary;
	}

	public void setValue(String interval)
	{
		mInterval.set(interval);
		if (shouldPersist())
			persistString(mInterval.toString());
		updateCalendar();
	}

	protected boolean updateCalendar()
	{
		final String next = mInterval.toString();
		if (!next.equals(mLastInterval))
		{
			try
			{
				mCalendar = mInterval.toCalendar();
			}
			catch (ParseException e)
			{
				return false;
			}
			mLastInterval = next;
		}
		return true;
	}

	protected void updateInterval()
	{
		String m = mMonthsEdit.getEditableText().toString();
		String d = mDaysEdit.getEditableText().toString();
		String h = mHoursEdit.getEditableText().toString();
		String s = String.format("%02d:%02d:%02d", m, d, h);
		setValue(s);
	}

	protected void updateTexts()
	{
		mMonthsEdit.setText(String.format("%02d", mInterval.getPart(DateItem.MONTHS)));
		mDaysEdit.setText(String.format("%02d", mInterval.getPart(DateItem.DAYS)));
		mHoursEdit.setText(String.format("%02d", mInterval.getPart(DateItem.HOURS)));
	}

	@Override
	protected void onDialogClosed(boolean positiveResult)
	{
		super.onDialogClosed(positiveResult);

		if (!positiveResult)
			return;
		if (shouldPersist())
			persistString(mInterval.toString());

		notifyChanged();
	}
}