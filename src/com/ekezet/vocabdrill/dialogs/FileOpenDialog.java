package com.ekezet.vocabdrill.dialogs;

import java.io.File;
import java.io.FileFilter;
import java.util.Locale;

import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.ekezet.vocabdrill.R;

public class FileOpenDialog extends OpenDialog
{

	private FileFilter mDirFilter = new FileFilter()
	{
		@Override
		public boolean accept(File pathname)
		{
			if (pathname.isDirectory()
				&& !pathname.isFile()
				&& !pathname.isHidden())
				return true;
			return false;
		}
	};

	private FileFilter mFileFilter = new FileFilter()
	{
		@Override
		public boolean accept(File pathname)
		{
			if (pathname.isDirectory() && !pathname.isFile())
				return false;
			if (pathname.getName()
				.toLowerCase(Locale.getDefault())
				.endsWith(".kvtml"))
				return true;
			return false;
		}
	};

	public class FileDialogEntry extends DialogEntry
	{
		private File mFile = null;
		private String mName = null;

		public FileDialogEntry(String path)
		{
			super(path);
			mFile = new File(path);
			if (mFile == null || !mFile.exists())
				throw new NullPointerException(
					String.format("Path not found: '%s'", path));
			mPath = mFile.getAbsolutePath();
			mName = mFile.getName();
		}

		@Override
		public String getName()
		{
			return mName;
		}

		@Override
		public String getParent()
		{
			return mFile.getParent();
		}

		@Override
		public DialogEntry[] getFileList()
		{
			File[] list = mFile.listFiles(mFileFilter);
			if (list == null || list.length == 0)
				return null;
			DialogEntry[] result = new DialogEntry[list.length];
			for (int i = 0, I = list.length; i < I; i++)
				result[i] = new FileDialogEntry(list[i].getPath());
			return result;
		}

		@Override
		public DialogEntry[] getFolderList()
		{
			File[] list = mFile.listFiles(mDirFilter);
			if (list == null || list.length == 0)
				return null;
			DialogEntry[] result = new DialogEntry[list.length];
			for (int i = 0, I = list.length; i < I; i++)
				result[i] = new FileDialogEntry(list[i].getPath());
			return result;
		}

		@Override
		public boolean canRead()
		{
			return mFile.canRead();
		}

		@Override
		public boolean isDirectory()
		{
			return mFile.isDirectory();
		}

		@Override
		protected boolean isSelected()
		{
			return !this.isDirectory();
		}

		@Override
		protected boolean exists()
		{
			return mFile.exists();
		}
	}

	protected void initialize(Bundle savedInstanceState)
	{
		mStoragePath = Environment
			.getExternalStorageDirectory()
			.getAbsolutePath();
		mDefaultPath = mStoragePath;
		final String path = getIntent().getStringExtra(INTENT_PATH_PARAMETER);
		if (path != null)
			mCurrentEntry = newEntry(path);
		else
			mCurrentEntry = newEntry(mPrefs.getString(mLastPathPrefName, mDefaultPath));
		// check configuration change
		if (savedInstanceState != null)
			mCurrentEntry = newEntry(savedInstanceState.getString(mLastPathPrefName));
		if (!mCurrentEntry.exists())
			// try jumping to root coz it shall exist
			mCurrentEntry = newEntry("/");
	}

	@Override
	protected DialogEntry newEntry(String path)
	{
		return new FileDialogEntry(path);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		final MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.open_dialog_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
			case R.id.option_goto_ext:
				updateList(newEntry(mStoragePath));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected String getLastPathPrefName()
	{
		return "file_browser_default_dir";
	}
}