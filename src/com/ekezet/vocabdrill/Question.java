package com.ekezet.vocabdrill;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import com.ekezet.vocabdrill.kvtml.Kvtml;

public final class Question
{
	private ArrayList<Kvtml.Entry> mQuestionEntries = null;
	private Kvtml.Entry mSolution = null;
	private int mSolutionIndex = -1;

	public Question(HashMap<String, Kvtml.Entry> entries, Kvtml.Entry solution, Random rnd)
	{
		initialize(entries, solution, rnd);
	}

	public Question(HashMap<String, Kvtml.Entry> entries, Kvtml.Entry solution)
	{
		Random rnd = new Random(System.nanoTime() * Double.doubleToLongBits(Math.random()));
		initialize(entries, solution, rnd);
	}

	private void initialize(HashMap<String, Kvtml.Entry> entries, Kvtml.Entry solution, Random rnd)
	{
		mQuestionEntries = new ArrayList<Kvtml.Entry>();
		mQuestionEntries.addAll(entries.values());
		if (rnd != null)
			Collections.shuffle(mQuestionEntries, rnd);
		mSolution = solution;
		mSolutionIndex = mQuestionEntries.indexOf(solution);
	}

	public ArrayList<Kvtml.Entry> getQuestionEntries()
	{
		return mQuestionEntries;
	}

	public Kvtml.Entry getSolution()
	{
		return mSolution;
	}

	public String getQuestionEntry(int index)
	{
		return mQuestionEntries.get(index).id;
	}

	/**
	 * Index of the solution within the question entries array.
	 *
	 * @return
	 */
	public int getSolutionIndex()
	{
		return mSolutionIndex;
	}

	public String getSolutionEntryId()
	{
		return mSolution.id;
	}
}