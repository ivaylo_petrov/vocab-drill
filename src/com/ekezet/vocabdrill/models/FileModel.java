package com.ekezet.vocabdrill.models;

import android.content.ContentValues;
import android.database.Cursor;


public class FileModel extends BaseModel
{
	public static final String[] fields = new String[] { "_id", "name", "opened" };
	
	public static final String CREATE_STATEMENT = "CREATE TABLE files (_id INTEGER PRIMARY KEY, name TEXT, opened NUMERIC)";
	
	private String mName;
	private int mOpened;
	
	public FileModel(long id, String name, int opened)
	{
		super(id);
		setName(name);
		setOpened(opened);
	}
	
	public String getName()
	{
		return mName;
	}
	
	public void setName(String name)
	{
		mName = name;
	}
	
	public int getOpened()
	{
		return mOpened;
	}
	
	public void setOpened(int opened)
	{
		mOpened = opened;
	}
	
	@Override
	public ContentValues getValues(boolean includeId)
	{
		ContentValues values = super.getValues(includeId);
		values.put("name", getName());
		values.put("opened", getName());
		return values;
	}
	
	@Override
	public FileModel populate(Cursor data, boolean idIncluded)
	{
		super.populate(data, idIncluded);
		
		if (data == null)
			return null;
		
		int start = 1;
		if (idIncluded)
		{
			setId(data.getLong(0));
			start = 0;
		}
		
		setName(data.getString(1 - start));
		setOpened(data.getInt(2 - start));
		return this;
	}
}
