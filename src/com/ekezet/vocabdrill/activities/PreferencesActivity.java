package com.ekezet.vocabdrill.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.view.WindowManager.LayoutParams;
import android.widget.Toast;

import com.dropbox.client2.session.AccessTokenPair;
import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.dialogs.NewsDialog;
import com.ekezet.vocabdrill.helpers.DbxTools;
import com.ekezet.vocabdrill.helpers.StatsOpenHelper;
import com.ekezet.vocabdrill.widgets.IntervalPreference;
import com.ekezet.vocabdrill.widgets.SeekBarPreference;

public class PreferencesActivity extends PreferenceActivity
{
	private Preference mDbxAuthPreference = null;
	private Preference mClearStatsPreference = null;
	private Preference mClearCachePreference = null;
	private SeekBarPreference mMaxRecentFilesNumPreference = null;

	private final OnPreferenceClickListener mDbxAuthStarter = new OnPreferenceClickListener()
	{
		@Override
		public boolean onPreferenceClick(Preference preference)
		{
			DbxTools.startAuthentication(PreferencesActivity.this);
			return true;
		}
	};

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getWindow().setFlags(LayoutParams.FLAG_FULLSCREEN,
			LayoutParams.FLAG_FULLSCREEN);
		addPreferencesFromResource(R.xml.preferences);

		// choice number preference
		SeekBarPreference maxItemsPreference = (SeekBarPreference) findPreference("choiceNumber");
		maxItemsPreference.setDefaultValue(Config.choiceNumber);
		maxItemsPreference.setSummaryPrefixText(R.string.summary_current_number);
		maxItemsPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener()
		{
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue)
			{
				Config.choiceNumber = (Integer) newValue;
				return true;
			}
		});

		ListPreference lstpref = null;

		// question delay preference
		lstpref = (ListPreference) findPreference("questionDelay");
		lstpref.setValue(String.valueOf(Config.questionDelay));
		lstpref.setSummary(String.valueOf(Config.questionDelay) + " ms");
		lstpref.setOnPreferenceChangeListener(new OnPreferenceChangeListener()
		{
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue)
			{
				preference.setSummary(String.valueOf(newValue) + " ms");
				Config.questionDelay = Long.valueOf(String.valueOf(newValue));
				return true;
			}
		});

		// setup Leitner-System preferences
		IntervalPreference ivpref;
		for (int i = 0, I = Config.practiceIntervals.length; i < I; i++)
		{
			ivpref = (IntervalPreference) findPreference(String.format("leitner%s", i + 1));
			ivpref.setDefaultValue(Config.practiceIntervals[i]);
			ivpref.setOnPreferenceChangeListener(new OnPreferenceChangeListener()
			{
				@Override
				public boolean onPreferenceChange(Preference preference, Object newValue)
				{
					int idx = Integer.valueOf(String.valueOf(preference.getKey()).split("leitner")[1]);
					Config.practiceIntervals[idx - 1] = String.valueOf(newValue);
					return true;
				}
			});
		}

		mMaxRecentFilesNumPreference = (SeekBarPreference) findPreference("maxRecentFilesNum");
		mMaxRecentFilesNumPreference.setSummaryPrefixText(R.string.summary_current_number);
		mMaxRecentFilesNumPreference.setDefaultValue(Config.maxRecentFilesNum);
		mMaxRecentFilesNumPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener()
		{
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue)
			{
				Config.maxRecentFilesNum = (Integer) newValue;
				return true;
			}
		});

		CheckBoxPreference cbpref = null;

		cbpref = (CheckBoxPreference) findPreference("forceChoiceNumber");
		cbpref.setChecked(Config.forceChoiceNumber);
		cbpref.setOnPreferenceChangeListener(new OnPreferenceChangeListener()
		{
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue)
			{
				Config.forceChoiceNumber = Boolean.valueOf(String.valueOf(newValue));
				return true;
			}
		});

		cbpref = (CheckBoxPreference) findPreference("scanDirectories");
		cbpref.setChecked(Config.scanDirectories);
		cbpref.setOnPreferenceChangeListener(new OnPreferenceChangeListener()
		{
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue)
			{
				Config.scanDirectories = String.valueOf(newValue).equals("1");
				return true;
			}
		});

		// confirmations settings
		cbpref = (CheckBoxPreference) findPreference("confirmExit");
		cbpref.setChecked(Config.confirmExit);
		cbpref.setOnPreferenceChangeListener(new OnPreferenceChangeListener()
		{
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue)
			{
				Config.confirmExit = String.valueOf(newValue).equals("1");
				return true;
			}
		});

		cbpref = (CheckBoxPreference) findPreference("confirmProgress");
		cbpref.setChecked(Config.confirmProgress);
		cbpref.setOnPreferenceChangeListener(new OnPreferenceChangeListener()
		{
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue)
			{
				Config.confirmProgress = String.valueOf(newValue).equals("1");
				return true;
			}
		});

		cbpref = (CheckBoxPreference) findPreference("confirmRestart");
		cbpref.setChecked(Config.confirmRestart);
		cbpref.setOnPreferenceChangeListener(new OnPreferenceChangeListener()
		{
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue)
			{
				Config.confirmRestart = String.valueOf(newValue).equals("1");
				return true;
			}
		});

		// clear stats
		mClearStatsPreference = findPreference("clearStats");
		mClearStatsPreference.setOnPreferenceClickListener(new OnPreferenceClickListener()
		{
			@Override
			public boolean onPreferenceClick(Preference preference)
			{
				new AlertDialog.Builder(PreferencesActivity.this)
					.setTitle(R.string.pref_reset_stats)
					.setMessage(R.string.message_really_clear_global_stats)
					.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which)
						{
							StatsOpenHelper stats = StatsOpenHelper.getInstance(PreferencesActivity.this, 0);
							stats.clearAllStats();
							stats.close();
							updateScreen();
						}
					}).setNegativeButton(R.string.button_no, null).show();
				return true;
			}
		});

		// clear cache
		mClearCachePreference = findPreference("clearCache");
		mClearCachePreference.setOnPreferenceClickListener(new OnPreferenceClickListener()
		{
			@Override
			public boolean onPreferenceClick(Preference preference)
			{
				new AlertDialog.Builder(PreferencesActivity.this)
					.setTitle(R.string.pref_clear_cache)
					.setMessage(R.string.message_really_clear_cache)
					.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which)
						{
							Config.clearCacheDir(getApplicationContext());
							updateScreen();
						}
					}).setNegativeButton(R.string.button_no, null).show();
				return true;
			}
		});

		Preference pref = null;

		// reset settings
		pref = findPreference("clearSettings");
		pref.setOnPreferenceClickListener(new OnPreferenceClickListener()
		{
			@Override
			public boolean onPreferenceClick(Preference preference)
			{
				new AlertDialog.Builder(PreferencesActivity.this)
					.setTitle(R.string.pref_clear_settings)
					.setMessage(R.string.message_really_clear_settings)
					.setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which)
						{
							final SharedPreferences prefs = getApplicationContext().getSharedPreferences(Config.PREFS_FILE, MODE_PRIVATE);
							SharedPreferences.Editor edit = prefs.edit();
							edit.clear().commit();
							updateScreen();
						}
					}).setNegativeButton(R.string.button_no, null).show();
				return true;
			}
		});

		// dropbox auth launcher
		mDbxAuthPreference = findPreference("dbxAuth");

		// what's new screen
		pref = findPreference("showNews");
		pref.setOnPreferenceClickListener(new OnPreferenceClickListener()
		{
			@Override
			public boolean onPreferenceClick(Preference preference)
			{
				startActivity(new Intent(PreferencesActivity.this, NewsDialog.class));
				return true;
			}
		});

		// about screen
		pref = findPreference("aboutVocabdrill");
		pref.setOnPreferenceClickListener(new OnPreferenceClickListener()
		{
			@Override
			public boolean onPreferenceClick(Preference preference)
			{
				startActivity(new Intent(PreferencesActivity.this, AboutActivity.class));
				return true;
			}
		});

		updateScreen();
	}

	protected void updateScreen()
	{
		if (mClearStatsPreference != null)
		{
			StatsOpenHelper stats = StatsOpenHelper.getInstance(PreferencesActivity.this, 0);
			mClearStatsPreference.setEnabled(stats.hasStats());
			stats.close();
		}

		if (mClearCachePreference != null)
			mClearCachePreference.setEnabled(!Config.isCacheDirEmpty());

		if (mDbxAuthPreference == null)
			return;

		if (!DbxTools.isLinked())
		{
			mDbxAuthPreference.setTitle(R.string.pref_dropbox_connect);
			mDbxAuthPreference.setSummary("");
			mDbxAuthPreference.setOnPreferenceClickListener(mDbxAuthStarter);
			return;
		}

		mDbxAuthPreference.setTitle(R.string.pref_dropbox_disconnect);
		mDbxAuthPreference.setSummary(R.string.summary_dropbox_disconnect);
		mDbxAuthPreference.setOnPreferenceClickListener(new OnPreferenceClickListener()
		{
			@Override
			public boolean onPreferenceClick(Preference preference)
			{
				try
				{
					DbxTools.unlink();
				}
				catch (Exception e)
				{
					return false;
				}
				updateScreen();
				return true;
			}
		});
	}

	@Override
	protected void onDestroy()
	{
		Config.save();
		super.onDestroy();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		try
		{
			if (DbxTools.finishAuthentication())
			{
				final SharedPreferences.Editor edit = Config.getPrefs().edit();
				final AccessTokenPair tokens = DbxTools.getAccessTokenPair();
				edit.putString(DbxTools.PREF_DBX_TOKEN_KEY, tokens.key);
				edit.putString(DbxTools.PREF_DBX_TOKEN_SECRET, tokens.secret);
				edit.commit();

				if (!Config.isDropboxSuccessDialogMuted())
				{
					new AlertDialog.Builder(PreferencesActivity.this)
						.setTitle(R.string.title_dropbox_success)
						.setMessage(R.string.message_dropbox_success)
						.setPositiveButton(R.string.button_ok, new OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int which)
							{
								Config.setDropboxSuccessDialogMuted(true);
								Config.save();
								dialog.dismiss();
							}
						})
						.create().show();
				}
			}
		}
		catch (IllegalStateException e)
		{
			Toast.makeText(getApplicationContext(), "Error.", Toast.LENGTH_LONG).show();
		}
		updateScreen();
	}
}